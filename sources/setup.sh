#!/bin/zsh

apt update
apt install -y \
  net-tools \
  iptables \
  git \
  dnsutils \
  netcat \
  telnet \
  whois \
  vim \
  curl \
  wget \
  openvpn \
  python3-pip \
  zsh \
  &&

pip install \
  httpstat \
  speedtest-cli \
  &&

sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k

curl -fsSL https://get.docker.com | sh -
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose