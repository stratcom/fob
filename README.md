# Forward Operating Base

<!-- vscode-markdown-toc -->
* 1. [Overview](#Overview)
* 2. [Dependencies](#Dependencies)
* 3. [Installation](#Installation)
* 4. [Tasking](#Tasking)
* 5. [Monitoring](#Monitoring)
* 6. [TBD](#TBD)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->
##  1. <a name='Overview'></a>Overview

This is a penetration testing project for a multi-site coordinated flood attack simulation. Basic safeguards are installed within the VM supplied with the project, such as:
- TOR
- Privoxy
- OpenVPN

The underlying principle is that all tooling for the attack is self-contained within the VM used and the VM has the ability to independently proxy or VPN, without affecting the host machine and so it doesn't require advanced operating skills.

##  2. <a name='Dependencies'></a>Dependencies

The project currently works on:
- Linux
- Windows 7+
- MacOS 10+

It uses:
- [Virtualbox 6.1+](https://www.virtualbox.org/wiki/Downloads)
- [Vagrant 2.2+](https://www.vagrantup.com/downloads)

Once you have installed both Virtualbox and Vagrant, you also need to install [the vbguest plugin for Vagrant](https://github.com/dotless-de/vagrant-vbguest#vagrant--13).

##  3. <a name='Installation'></a>Installation

[Download this project zip](https://gitlab.com/stratcom/fob/-/archive/main/fob-main.zip) and extract it to a folder of your choice on your hard drive

OR

Clone this project with `git` to a folder of your preference.

1. `cd` into the folder of the project (e.g. `cd fob`)
2. `vagrant plugin install vagrant-vbguest`
3. `vagrant up fob`

You now need to wait for the VM to provision. This can take up to 20 minutes depending on your connection speed and machine performance.

When the provisioning is complete, the machine will be running the required services and will be ready for insruction.

##  4. <a name='Tasking'></a>Tasking

There is basic functionality built into the FOB VM for flood attacks. You can task for attacks by using the following commands.

- `vagrant ssh fob` will get you into the machine (you need to be in the folder where you started the machine from)
- `sudo su -` to switch to root INSIDE the machine. The tools are set up to run as root.
- `whatismycover` to show you the current whois record for your outbound connection. This should be different from your actual location.
  - If it's NOT, run `docker restart op-redmist_tor_1`, wait a couple seconds and re-run `whatismycover`. This will rotate your TOR gateway and should now show a different address.
- `httpflood DOMAIN IP` where `DOMAIN` is the website's domain and IP is one of the IPs of the website you're trying to flood. An example would be `httpflood google.com 142.250.186.110` to flood google.com on that IP for both port `80` and `443`. This will be executed through TOR so speed will be slower, but it will protect your host from identification.
  - You can start multiple of these easily with lining them up like so:
  ```
  httpflood google.com 1.2.3.4
  httpflood google.com 4.5.6.7
  ```
  - Don't forget to pass the site domain as most systems are protected by header-based load balancing, so if you just hit the IP it won't route your traffic to the destination.
  - The attack runs for `100000000` requests or `2 hours`, whichever completes first. Once completed, the container will self-terminate and disappear.
- `stophttpflood` will terminate ALL running flood attacks immediately.

##  5. <a name='Monitoring'></a>Monitoring

Currently the easy option for monitoring is to use the `docker` CLI command. When in the server under root (`sudo su -`), you can run `docker ps -a --format '{{.Names}}' | grep httpflood` which will show you payload containers.

You can then copy the name of the one you want to investigate and use it in:
```
docker logs CONTAINERNAME
```
This will show you the logs of the attack output from that container.
- Keep in mind, these logs are deleted when your container has finished the attack, so you can only track these while it's still in progress.

##  6. <a name='TBD'></a>TBD

This is a very early WIP state, to illustrate a packaged solution for penetration testing on a large scale. Further enhancements will be added, including a guide to use OpenVPN within the VM as well as linking up mulitple FOB hosts for a coordinated attack.