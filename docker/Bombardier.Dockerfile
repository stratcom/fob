FROM golang:alpine
RUN go install github.com/codesenberg/bombardier@latest
ENTRYPOINT ["bombardier"]