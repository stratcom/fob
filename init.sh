#!/bin/bash
bash /vagrant/sources/setup.sh

cp -r /vagrant/sources/home/. /root/
chmod 0644 \
  /root/.zshrc \
  &&
chsh -s /bin/zsh root

bash /vagrant/sources/stackstart.sh